import Link from 'next/link';
import styled from 'styled-components';
import Nav from './Nav';

const Logo = styled.h1`
   font-size: 4rem;
   background-color: #dc7171;
   margin-left: 2rem;
   position: relative;
   z-index: 2;
   transform: skew(-7deg);

   a {
      color: #fff;
      text-decoration: none;
      text-transform: uppercase;
      padding: .5rem 1rem;
   }
`;

const HeaderStyled = styled.header`
   .bar {
      border-bottom: 10px solid var(--black);
      display: grid;
      grid-template-columns: auto 1fr;
      justify-content: space-between;
      align-items: center;
   }

   .sub-bar {
      display: grid;
      grid-template-columns: 1fr auto;
      border-bottom: 2px solid var(--black);
   }
`;

export default function Header() {
   return (
      <HeaderStyled>
			<div className="bar">
				<Logo>
               <Link href="/">Sick Fits</Link>
            </Logo>
         </div>
         <div className="sub-bar">
            <p>Search</p>
         </div>
         <Nav />
      </HeaderStyled>
   );
}