import { createAuth } from '@keystone-next/auth';
import { config, createSchema } from '@keystone-next/keystone/schema';
import { withItemData, 	statelessSessions } from '@keystone-next/keystone/session';
import { ProductImage } from './schemas/ProductImage';
import { Product } from './schemas/Product';
import { User } from './schemas/User';
import 'dotenv/config';
import { insertSeedData } from './seed-data';

const databaseURL = process.env.DATABASE_URL || 'mongobd://localhost/keystone-sick-fits-tutorial';

const sessionConfig = {
	maxAge: 60 * 60 * 24 * 360, // How long they stay signed in
	secret: process.env.COOKIE_SECRET,
};

// @ts-ignore
const { withAuth } = createAuth({
	listKey: 'User',
	identityField: 'email',
	secretField: 'password',
	initFirstItem: {
		fields: ['name', 'email', 'password'],
		// TODO: Add in initial roles here
	}
});

export default withAuth(
	config({
		// @ts-ignore
		server: {
			cors: {
				origin: [process.env.FRONTEND_URL],
					credentials: true
			},
		},
		db: {
			adapter: 'mongoose',
			url: databaseURL,
			async onConnect(keystone) {
				if (process.argv.includes('--seed-data')) {
					await insertSeedData(keystone);
				}
			}
		},
		lists: createSchema({
			// Schema items go in here
			User,
			Product,
			ProductImage,
		}),
		ui: {
			// Show the UI only for users who pass this test
			isAccessAllowed: ({ session }) => 
				!!session?.data,
		},
		session: withItemData(statelessSessions(sessionConfig), {
			// GraphQL query
			User: `id name email`
		})
	})
);